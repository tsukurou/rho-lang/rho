//! `Null` and associated items.

use super::Type;

/// The type literal associated with this primitive type.
pub const TYPE: Type = Type::NULL;

/// The literal value `null`.
pub const NULL: Null = Null(());

/// `nulltype` - A unit type with a single literal, `null`.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash, Default)]
pub struct Null(());

impl Null {
    /// The type literal associated with this primitive type.
    pub const TYPE: Type = TYPE;

    /// The literal value `null`.
    pub const NULL: Null = NULL;
}
