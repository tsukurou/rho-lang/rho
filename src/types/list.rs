//! `List` and associated items.

use super::{Rc, Type};
use crate::value::Value;
use std::iter::FromIterator;
use std::ops;

/// The type literal for this primitive type.
pub const TYPE: Type = Type::LIST;

/// `list` - An ordered sequence of `Value`s.
///
/// The `+` operator and its in-place version `+=` are defined to concatenate two values to produce
/// a new one. This behavior is equivalent to the corresponding operators in Rho.
#[derive(Debug, PartialEq, Clone)]
pub struct List(Rc<Vec<Value>>);

/// A non-mutating iterator over the items in a list.
#[derive(Debug, Clone)]
pub struct Iter<'a>(std::slice::Iter<'a, Value>);

/// A mutating iterator over the items in a list.
#[derive(Debug)]
pub struct IterMut<'a>(std::slice::IterMut<'a, Value>);

impl List {
    /// The type literal for this primitive type.
    pub const TYPE: Type = TYPE;

    /// Creates a list with elements consumed from the given vector.
    pub fn from_vec(x: Vec<Value>) -> List {
        List(Rc::new(x))
    }

    /// Creates a list with elements cloned from the given slice.
    pub fn from_slice(x: &[Value]) -> List {
        List::from_vec(x.to_vec())
    }

    /// Creates a list with no elements.
    pub fn empty() -> List {
        List::from_vec(vec![])
    }

    /// Returns the number of items in this list.
    pub fn len(&self) -> usize {
        self.as_ref().len()
    }

    /// References the item at the given index if it exists.
    pub fn get(&self, idx: usize) -> Option<&Value> {
        self.as_ref().get(idx)
    }

    /// Mutably references the item at the given index if it exists.
    pub fn get_mut(&mut self, idx: usize) -> Option<&mut Value> {
        self.as_mut().get_mut(idx)
    }

    /// Appends the items yielded from the given iterator to this list in order.
    pub fn extend<I>(&mut self, iter: I)
    where
        I: IntoIterator<Item = Value>,
    {
        self.as_mut_vec().extend(iter.into_iter());
    }

    /// Returns a shared iterator over the items in this list.
    pub fn iter(&self) -> Iter {
        Iter(self.as_ref().iter())
    }

    /// Returns a mutating iterator over the items in this list.
    pub fn iter_mut(&mut self) -> IterMut {
        IterMut(self.as_mut().iter_mut())
    }

    /// Returns a shared reference to the inner `Vec`. This is an implementation detail and should
    /// not be made public.
    fn as_vec(&self) -> &Vec<Value> {
        self.0.as_ref()
    }

    /// Returns a shared slice of the list's contents.
    pub fn as_slice(&self) -> &[Value] {
        self.as_vec().as_slice()
    }

    /// Returns a mutable reference to the inner `Vec`, cloning if necessary to create a unique
    /// value. This is an implementation detail and should not be made public.
    fn as_mut_vec(&mut self) -> &mut Vec<Value> {
        Rc::make_mut(&mut self.0)
    }

    /// Returns a mutable slice of the list's contents.
    pub fn as_mut_slice(&mut self) -> &mut [Value] {
        self.as_mut_vec().as_mut_slice()
    }
}

impl<'a> From<&'a [Value]> for List {
    fn from(x: &'a [Value]) -> List {
        List::from_slice(x)
    }
}

impl From<Vec<Value>> for List {
    fn from(x: Vec<Value>) -> List {
        List::from_vec(x)
    }
}

impl FromIterator<Value> for List {
    fn from_iter<I: IntoIterator<Item = Value>>(iter: I) -> Self {
        List::from(iter.into_iter().collect::<Vec<_>>())
    }
}

impl AsRef<[Value]> for List {
    fn as_ref(&self) -> &[Value] {
        self.as_slice()
    }
}

impl AsMut<[Value]> for List {
    fn as_mut(&mut self) -> &mut [Value] {
        self.as_mut_slice()
    }
}

impl ops::Add for List {
    type Output = List;

    fn add(mut self, rhs: List) -> Self::Output {
        self += rhs;
        self
    }
}

impl ops::AddAssign for List {
    fn add_assign(&mut self, rhs: List) {
        self.extend(rhs.iter().cloned())
    }
}

impl ops::Index<usize> for List {
    type Output = Value;

    fn index(&self, idx: usize) -> &Value {
        self.get(idx).expect("Index out of bounds")
    }
}

impl ops::IndexMut<usize> for List {
    fn index_mut(&mut self, idx: usize) -> &mut Value {
        self.get_mut(idx).expect("Index out of bounds")
    }
}

impl<'a> IntoIterator for &'a List {
    type Item = &'a Value;
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a> IntoIterator for &'a mut List {
    type Item = &'a mut Value;
    type IntoIter = IterMut<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = &'a Value;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

impl<'a> Iterator for IterMut<'a> {
    type Item = &'a mut Value;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}
