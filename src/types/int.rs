//! `Int` and associated items.

use super::Type;
use std::cmp::Ordering;
use std::num::Wrapping;
use std::ops;

/// The type literal of this primitive type.
pub const TYPE: Type = Type::INT;

/// `int` - A 128-bit signed integer, with the same range as `i128`.
///
/// Arithmetic operations `+`, `-`, `*`, `/`, `%`, bitwise operations `>>`, `<<`, `&`, `|`, `^`,
/// `!`, and their respective assignment operators are defined on integer types. The operations
/// will wrap in the case of overflow, according to the behavior of the corresponding `wrapping_*`
/// function in `std`. The behavior of their respective operators in Rho will be equivalent.
///
/// # Examples
///
/// (TODO for when language is implemented)
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Hash)]
pub struct Int(Wrapping<i128>);

impl Int {
    /// The type literal of this primitive type.
    pub const TYPE: Type = TYPE;

    /// Converts the Rust integer to its equivalent value in Rho.
    pub const fn from_i128(x: i128) -> Int {
        Int(Wrapping(x))
    }

    /// Converts this value into its equivalent Rust integer.
    pub const fn to_i128(self) -> i128 {
        (self.0).0
    }
}

impl From<i128> for Int {
    fn from(x: i128) -> Int {
        Int::from_i128(x)
    }
}

impl From<Int> for i128 {
    fn from(x: Int) -> i128 {
        x.to_i128()
    }
}

impl ops::Add for Int {
    type Output = Int;

    fn add(self, rhs: Int) -> Self::Output {
        Int(self.0 + rhs.0)
    }
}

impl ops::AddAssign for Int {
    fn add_assign(&mut self, rhs: Int) {
        self.0 += rhs.0;
    }
}

impl ops::Sub for Int {
    type Output = Int;

    fn sub(self, rhs: Int) -> Self::Output {
        Int(self.0 - rhs.0)
    }
}

impl ops::SubAssign for Int {
    fn sub_assign(&mut self, rhs: Int) {
        self.0 -= rhs.0;
    }
}

impl ops::Mul for Int {
    type Output = Int;

    fn mul(self, rhs: Int) -> Self::Output {
        Int(self.0 * rhs.0)
    }
}

impl ops::MulAssign for Int {
    fn mul_assign(&mut self, rhs: Int) {
        self.0 *= rhs.0;
    }
}

impl ops::Div for Int {
    type Output = Int;

    fn div(self, rhs: Int) -> Self::Output {
        Int(self.0 / rhs.0)
    }
}

impl ops::DivAssign for Int {
    fn div_assign(&mut self, rhs: Int) {
        self.0 /= rhs.0;
    }
}

impl ops::Rem for Int {
    type Output = Int;

    fn rem(self, rhs: Int) -> Self::Output {
        Int(self.0 % rhs.0)
    }
}

impl ops::RemAssign for Int {
    fn rem_assign(&mut self, rhs: Int) {
        self.0 %= rhs.0;
    }
}

impl ops::Shl for Int {
    type Output = Int;

    fn shl(self, rhs: Int) -> Self::Output {
        match (rhs.0).0.cmp(&0) {
            Ordering::Greater => Int(self.0 << ((rhs.0).0 as usize)),
            Ordering::Less => Int(self.0 >> ((-rhs.0).0 as usize)),
            Ordering::Equal => self,
        }
    }
}

impl ops::ShlAssign for Int {
    fn shl_assign(&mut self, rhs: Int) {
        *self = *self << rhs;
    }
}

impl ops::Shr for Int {
    type Output = Int;

    fn shr(self, rhs: Int) -> Self::Output {
        match (rhs.0).0.cmp(&0) {
            Ordering::Greater => Int(self.0 >> ((rhs.0).0 as usize)),
            Ordering::Less => Int(self.0 << ((-rhs.0).0 as usize)),
            Ordering::Equal => self,
        }
    }
}

impl ops::ShrAssign for Int {
    fn shr_assign(&mut self, rhs: Int) {
        *self = *self >> rhs;
    }
}

impl ops::BitAnd for Int {
    type Output = Int;

    fn bitand(self, rhs: Int) -> Self::Output {
        Int(self.0 & rhs.0)
    }
}

impl ops::BitAndAssign for Int {
    fn bitand_assign(&mut self, rhs: Int) {
        self.0 &= rhs.0;
    }
}

impl ops::BitOr for Int {
    type Output = Int;

    fn bitor(self, rhs: Int) -> Self::Output {
        Int(self.0 | rhs.0)
    }
}

impl ops::BitOrAssign for Int {
    fn bitor_assign(&mut self, rhs: Int) {
        self.0 |= rhs.0;
    }
}

impl ops::BitXor for Int {
    type Output = Int;

    fn bitxor(self, rhs: Int) -> Self::Output {
        Int(self.0 ^ rhs.0)
    }
}

impl ops::BitXorAssign for Int {
    fn bitxor_assign(&mut self, rhs: Int) {
        self.0 ^= rhs.0;
    }
}

impl ops::Neg for Int {
    type Output = Int;

    fn neg(self) -> Self::Output {
        Int(-self.0)
    }
}

impl ops::Not for Int {
    type Output = Int;

    fn not(self) -> Self::Output {
        Int(!self.0)
    }
}
