//! `Function` and associated items.

use super::{Rc, Type};
use crate::value::Value;
use std::fmt;

/// The type literal for this primitive type.
pub const TYPE: Type = Type::FUNCTION;

/// `function` - A callable that transforms one value into another.
#[derive(Debug, Clone)]
pub struct Function(FunctionImpl);

#[derive(Clone)]
enum FunctionImpl {
    Rust(Rc<Fn(&Value) -> Value>),
}

impl Function {
    /// The type literal for this primitive type.
    pub const TYPE: Type = TYPE;

    /// Creates a function that calls into a Rust function.
    pub fn from_rust<F>(func: F) -> Function
    where
        F: Fn(&Value) -> Value + 'static,
    {
        Function(FunctionImpl::Rust(Rc::new(func)))
    }

    /// Calls the function.
    pub fn call(&self, value: &Value) -> Value {
        match &self.0 {
            FunctionImpl::Rust(func) => func(value),
        }
    }
}

impl PartialEq for Function {
    fn eq(&self, _other: &Function) -> bool {
        false
    }
}

impl fmt::Debug for FunctionImpl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            FunctionImpl::Rust(..) => f.write_str("Rust"),
        }
    }
}
