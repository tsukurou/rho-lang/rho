//! `Table` and associated items.

use super::{Rc, Str, Type};
use crate::value::Value;
use std::collections::HashMap;
use std::iter::FromIterator;
use std::ops;

/// The type literal for this primitive type.
pub const TYPE: Type = Type::TABLE;

/// `table` - A mapping of `Str` keys to `Value`s.
///
/// The `+` operator and its in-place version `+=` are defined to update the left-hand value with
/// the entries in the right-hand value. If both contain entries with the same key, the right-hand
/// takes precedence. This behavior is equivalent to the corresponding operators in Rho.
#[derive(Debug, PartialEq, Clone)]
pub struct Table(Rc<HashMap<Str, Value>>);

/// A non-mutating iterator over the entries in a table.
pub struct Iter<'a>(std::collections::hash_map::Iter<'a, Str, Value>);

/// A mutating iterator over the entries in a table.
pub struct IterMut<'a>(std::collections::hash_map::IterMut<'a, Str, Value>);

impl Table {
    /// The type literal for this primitive type.
    pub const TYPE: Type = TYPE;

    /// Constructs a table from a hash map of existing entries.
    pub fn from_hashmap(h: HashMap<Str, Value>) -> Table {
        Table(Rc::new(h))
    }

    /// Constructs a table with no entries.
    pub fn empty() -> Table {
        Table::from_hashmap(HashMap::new())
    }

    /// Returns the number of entries in this table.
    pub fn len(&self) -> usize {
        self.as_hashmap().len()
    }

    /// References the item at the given index if it exists.
    pub fn get(&self, idx: &Str) -> Option<&Value> {
        self.as_hashmap().get(idx)
    }

    /// Mutably references the item at the given index if it exists.
    pub fn get_mut(&mut self, idx: &Str) -> Option<&mut Value> {
        self.as_mut_hashmap().get_mut(idx)
    }

    /// Updates the entries in this table with the ones in the given iterator. Any entries with the
    /// same name will be replaced.
    pub fn extend<I>(&mut self, iter: I)
    where
        I: IntoIterator<Item = (Str, Value)>,
    {
        self.as_mut_hashmap().extend(iter.into_iter())
    }

    /// Returns a shared iterator over the entries in this list.
    pub fn iter(&self) -> Iter {
        Iter(self.as_hashmap().iter())
    }

    /// Returns a mutating iterator over the entries in this list.
    pub fn iter_mut(&mut self) -> IterMut {
        IterMut(self.as_mut_hashmap().iter_mut())
    }

    /// Gets a shared reference to the inner `HashMap` value. This is an implementation detail and
    /// should not be made public.
    fn as_hashmap(&self) -> &HashMap<Str, Value> {
        self.0.as_ref()
    }

    /// Gets a mutable reference to the inner `HashMap` value, cloning if needed to obtain a unique
    /// value. This is an implementation detail and should not be made public.
    fn as_mut_hashmap(&mut self) -> &mut HashMap<Str, Value> {
        Rc::make_mut(&mut self.0)
    }
}

impl FromIterator<(Str, Value)> for Table {
    fn from_iter<I: IntoIterator<Item = (Str, Value)>>(iter: I) -> Self {
        Table(Rc::new(iter.into_iter().collect()))
    }
}

impl ops::Add for Table {
    type Output = Table;

    fn add(mut self, rhs: Table) -> Self::Output {
        self += rhs;
        self
    }
}

impl ops::AddAssign for Table {
    fn add_assign(&mut self, rhs: Table) {
        self.extend(rhs.iter().map(|(k, v)| (k.clone(), v.clone())))
    }
}

impl<'a> IntoIterator for &'a Table {
    type Item = (&'a Str, &'a Value);
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a> IntoIterator for &'a mut Table {
    type Item = (&'a Str, &'a mut Value);
    type IntoIter = IterMut<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = (&'a Str, &'a Value);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

impl<'a> Iterator for IterMut<'a> {
    type Item = (&'a Str, &'a mut Value);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}
