//! `Str` and associated items.

use super::{Rc, Type};
use std::ops;

/// The type literal for this primitive type.
pub const TYPE: Type = Type::STR;

/// `str` - A Unicode string.
///
/// The `+` operator and its in-place version `+=` are defined to concatenate two values to produce
/// a new one. This behavior is equivalent to the corresponding operators in Rho.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub struct Str(Rc<String>);

/// A non-mutating iterator over the characters in the string.
#[derive(Debug, Clone)]
pub struct Iter<'a>(std::str::Chars<'a>);

impl Str {
    /// The type literal for this primitive type.
    pub const TYPE: Type = TYPE;

    /// Creates a new string from the given string value..
    pub fn from_string(s: String) -> Str {
        Str(Rc::new(s))
    }

    /// Creates a new string copied from the given string slice.
    pub fn from_str(s: &str) -> Str {
        Str::from_string(s.to_string())
    }

    /// Creates a new, empty string.
    pub fn empty() -> Str {
        Str::from_string(String::new())
    }

    /// Returns the number of characters in this string.
    pub fn len(&self) -> usize {
        self.as_ref().len()
    }

    /// Appends the characters yielded from the given iterator to this string in order.
    pub fn extend<I>(&mut self, iter: I)
    where
        I: IntoIterator<Item = char>,
    {
        self.as_mut_string().extend(iter.into_iter())
    }

    /// Returns a shared iterator over the characters in this string.
    pub fn iter(&self) -> Iter {
        Iter(self.as_ref().chars())
    }

    /// Gets a shared reference to the inner `String` value. This is an implementation detail and
    /// should not be made public.
    fn as_string(&self) -> &String {
        self.0.as_ref()
    }

    /// Gets a shared reference to the contents of this string as a string slice.
    pub fn as_str(&self) -> &str {
        self.as_string().as_str()
    }

    /// Gets a mutable reference to the inner `String` value, cloning if needed to produce a unique
    /// value. This is an implementation detail and should not be made public.
    fn as_mut_string(&mut self) -> &mut String {
        Rc::make_mut(&mut self.0)
    }

    /// Gets a mutable reference to the contents of this string as a string slice.
    pub fn as_mut_str(&mut self) -> &mut str {
        self.as_mut_string().as_mut_str()
    }
}

impl<'a> From<&'a str> for Str {
    fn from(x: &'a str) -> Str {
        Str::from_str(x)
    }
}

impl From<String> for Str {
    fn from(x: String) -> Str {
        Str::from_string(x)
    }
}

impl AsRef<str> for Str {
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl AsMut<str> for Str {
    fn as_mut(&mut self) -> &mut str {
        self.as_mut_str()
    }
}

impl ops::Add for Str {
    type Output = Str;

    fn add(mut self, rhs: Str) -> Self::Output {
        self += rhs;
        self
    }
}

impl ops::AddAssign for Str {
    fn add_assign(&mut self, rhs: Str) {
        self.extend(rhs.iter())
    }
}

impl<'a> IntoIterator for &'a Str {
    type Item = char;
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}
