//! `Float` and associated items.

use super::Type;
use std::ops;

/// The type literal for this primitive type.
pub const TYPE: Type = Type::FLOAT;

/// `float` - A 64-bit, double-precision floating point number.
///
/// Arithmetic operations `+`, `-`, `*`, `/`, `%` and their corresponding assignment operators are
/// defined on floating-point types. The behavior of their respective operators in Rho will be
/// equivalent.
///
/// # Examples
///
/// (TODO for when language is implemented)
#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct Float(f64);

impl Float {
    /// The type literal for this primitive type.
    pub const TYPE: Type = TYPE;

    /// Creates a float with a value equivalent to the given value in Rust, without loss of
    /// precision.
    pub const fn from_f64(x: f64) -> Float {
        Float(x)
    }

    /// Creates a float with a value equivalent to the given value in Rust, without loss of
    /// precision.
    pub const fn from_f32(x: f32) -> Float {
        Float::from_f64(x as f64)
    }

    /// Gets the value equivalent to this value in Rust, without loss of
    /// precision.
    pub const fn to_f64(self) -> f64 {
        self.0
    }

    /// Gets the value equivalent to this value in Rust, with some loss of
    /// precision.
    pub const fn to_f32(self) -> f32 {
        self.to_f64() as f32
    }
}

impl From<f64> for Float {
    fn from(x: f64) -> Float {
        Float::from_f64(x)
    }
}

impl From<Float> for f64 {
    fn from(x: Float) -> f64 {
        x.to_f64()
    }
}

impl From<f32> for Float {
    fn from(x: f32) -> Float {
        Float::from_f32(x)
    }
}

impl From<Float> for f32 {
    fn from(x: Float) -> f32 {
        x.to_f32()
    }
}

impl ops::Add for Float {
    type Output = Float;

    fn add(self, rhs: Float) -> Self::Output {
        Float(self.0 + rhs.0)
    }
}

impl ops::AddAssign for Float {
    fn add_assign(&mut self, rhs: Float) {
        self.0 += rhs.0;
    }
}

impl ops::Sub for Float {
    type Output = Float;

    fn sub(self, rhs: Float) -> Self::Output {
        Float(self.0 - rhs.0)
    }
}

impl ops::SubAssign for Float {
    fn sub_assign(&mut self, rhs: Float) {
        self.0 -= rhs.0;
    }
}

impl ops::Mul for Float {
    type Output = Float;

    fn mul(self, rhs: Float) -> Self::Output {
        Float(self.0 * rhs.0)
    }
}

impl ops::MulAssign for Float {
    fn mul_assign(&mut self, rhs: Float) {
        self.0 *= rhs.0;
    }
}

impl ops::Div for Float {
    type Output = Float;

    fn div(self, rhs: Float) -> Self::Output {
        Float(self.0 / rhs.0)
    }
}

impl ops::DivAssign for Float {
    fn div_assign(&mut self, rhs: Float) {
        self.0 /= rhs.0;
    }
}

impl ops::Rem for Float {
    type Output = Float;

    fn rem(self, rhs: Float) -> Self::Output {
        Float(self.0 % rhs.0)
    }
}

impl ops::RemAssign for Float {
    fn rem_assign(&mut self, rhs: Float) {
        self.0 %= rhs.0;
    }
}

impl ops::Neg for Float {
    type Output = Float;

    fn neg(self) -> Self::Output {
        Float(-self.0)
    }
}
