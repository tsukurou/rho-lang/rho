//! `Type` and associated items.

/// The type literal `nulltype`.
pub const NULL: Type = Type::NULL;

/// The type literal `bool`.
pub const BOOL: Type = Type::BOOL;

/// The type literal `int`.
pub const INT: Type = Type::INT;

/// The type literal `float`.
pub const FLOAT: Type = Type::FLOAT;

/// The type literal `bytes`.
pub const BYTES: Type = Type::BYTES;

/// The type literal `str`.
pub const STR: Type = Type::STR;

/// The type literal `list`.
pub const LIST: Type = Type::LIST;

/// The type literal `table`.
pub const TABLE: Type = Type::TABLE;

/// The type literal `function`.
pub const FUNCTION: Type = Type::FUNCTION;

/// The type literal `type` (also the type literal for this primitive type).
pub const TYPE: Type = Type::TYPE;

/// `type` - an enumerated type with literals for all of the builtin types.
#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Type(TypeValue);

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
enum TypeValue {
    Null,
    Bool,
    Int,
    Float,
    Bytes,
    Str,
    List,
    Table,
    Function,
    Type,
}

impl Type {
    /// The type literal `nulltype`.
    pub const NULL: Type = Type(TypeValue::Null);

    /// The type literal `bool`.
    pub const BOOL: Type = Type(TypeValue::Bool);

    /// The type literal `int`.
    pub const INT: Type = Type(TypeValue::Int);

    /// The type literal `float`.
    pub const FLOAT: Type = Type(TypeValue::Float);

    /// The type literal `bytes`.
    pub const BYTES: Type = Type(TypeValue::Bytes);

    /// The type literal `str`.
    pub const STR: Type = Type(TypeValue::Str);

    /// The type literal `list`.
    pub const LIST: Type = Type(TypeValue::List);

    /// The type literal `table`.
    pub const TABLE: Type = Type(TypeValue::Table);

    /// The type literal `function`.
    pub const FUNCTION: Type = Type(TypeValue::Function);

    /// The type literal `type` (also the type literal for this primitive type).
    pub const TYPE: Type = Type(TypeValue::Type);

    /// Returns the name of the type's literal value in the language.
    ///
    /// - `Null` => `nulltype`
    /// - `Bool` => `bool`
    /// - `Int` => `int`
    ///
    /// etc ...
    pub fn literal_name(&self) -> &'static str {
        match self.0 {
            TypeValue::Null => "nulltype",
            TypeValue::Bool => "bool",
            TypeValue::Int => "int",
            TypeValue::Float => "float",
            TypeValue::Bytes => "bytes",
            TypeValue::Str => "str",
            TypeValue::List => "list",
            TypeValue::Table => "table",
            TypeValue::Function => "function",
            TypeValue::Type => "type",
        }
    }
}
