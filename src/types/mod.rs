//! Language type definitions.
//!
//! The Rho langauge has 10 primitive types: `null`, `bool`, `int`, `float`, `bytes`, `str`,
//! `list`, `table`, `function`, `type`. Each one is implemented in Rust as its own type,
//! with the type name modified to fit Rust naming standards.
//!
//! Currently, there is no support for struct-like named types.

pub mod bool;
pub mod bytes;
pub mod float;
pub mod function;
pub mod int;
pub mod list;
pub mod null;
pub mod str;
pub mod table;
pub mod ty;

pub use self::bool::Bool;
pub use self::bytes::Bytes;
pub use self::float::Float;
pub use self::function::Function;
pub use self::int::Int;
pub use self::list::List;
pub use self::null::Null;
pub use self::str::Str;
pub use self::table::Table;
pub use self::ty::Type;

#[cfg(not(feature = "atomic"))]
type Rc<T> = std::rc::Rc<T>;
#[cfg(feature = "atomic")]
type Rc<T> = std::sync::Arc<T>;
