//! `Bytes` and associated items.

use super::{Rc, Type};
use std::iter::FromIterator;
use std::ops;

/// The type literal for this primitive type.
pub const TYPE: Type = Type::BYTES;

/// `bytes` - A string of raw bytes.
///
/// The `+` operator and its in-place version `+=` are defined to concatenate two values to produce
/// a new one. This behavior is equivalent to the corresponding operators in Rho.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub struct Bytes(Rc<Vec<u8>>);

/// A non-mutating iterator over the bytes in the string.
pub struct Iter<'a>(std::slice::Iter<'a, u8>);

/// A mutating iterator over the bytes in the string.
pub struct IterMut<'a>(std::slice::IterMut<'a, u8>);

impl Bytes {
    /// The type literal for this primitive type.
    pub const TYPE: Type = TYPE;

    /// Creates a bytestring from the given vector of byte values.
    pub fn from_vec(x: Vec<u8>) -> Bytes {
        Bytes(Rc::new(x))
    }

    /// Creates a bytestring copied from the given slice of byte values.
    pub fn from_slice(x: &[u8]) -> Bytes {
        Bytes::from_vec(x.to_vec())
    }

    /// Creates a new, empty bytestring.
    pub fn empty() -> Bytes {
        Bytes::from_vec(vec![])
    }

    /// Returns the number of bytes in this bytestring.
    pub fn len(&self) -> usize {
        self.as_ref().len()
    }

    /// References the byte at the given index if it exists.
    pub fn get(&self, idx: usize) -> Option<&u8> {
        self.as_ref().get(idx)
    }

    /// Mutably references the byte at the given index if it exists.
    pub fn get_mut(&mut self, idx: usize) -> Option<&mut u8> {
        self.as_mut().get_mut(idx)
    }

    /// Appends the bytes yielded from the given iterator to this bytestring in order.
    pub fn extend<I>(&mut self, iter: I)
    where
        I: IntoIterator<Item = u8>,
    {
        self.as_mut_vec().extend(iter.into_iter());
    }

    /// Returns a shared iterator over the bytes in this string.
    pub fn iter(&self) -> Iter {
        Iter(self.as_ref().iter())
    }

    /// Returns a mutating iterator over the bytes in this string.
    pub fn iter_mut(&mut self) -> IterMut {
        IterMut(self.as_mut().iter_mut())
    }

    /// Gets a shared reference to the inner `Vec`. This is an implementation detail and should not
    /// be made public.
    fn as_vec(&self) -> &Vec<u8> {
        self.0.as_ref()
    }

    /// Gets a shared reference to the bytes in this value.
    pub fn as_slice(&self) -> &[u8] {
        self.as_vec().as_slice()
    }

    /// Gets a mutable reference to the inner `Vec`, cloning if necessary to obtain a unique value.
    /// This is an implementation detaikl and should not be made public.
    fn as_mut_vec(&mut self) -> &mut Vec<u8> {
        Rc::make_mut(&mut self.0)
    }

    /// Gets a mutable reference to the bytes in this value.
    pub fn as_mut_slice(&mut self) -> &mut [u8] {
        self.as_mut_vec().as_mut_slice()
    }
}

impl<'a> From<&'a [u8]> for Bytes {
    fn from(x: &'a [u8]) -> Bytes {
        Bytes::from_slice(x)
    }
}

impl From<Vec<u8>> for Bytes {
    fn from(x: Vec<u8>) -> Bytes {
        Bytes::from_vec(x)
    }
}

impl FromIterator<u8> for Bytes {
    fn from_iter<I: IntoIterator<Item = u8>>(iter: I) -> Self {
        Bytes::from(iter.into_iter().collect::<Vec<_>>())
    }
}

impl AsRef<[u8]> for Bytes {
    fn as_ref(&self) -> &[u8] {
        self.as_slice()
    }
}

impl AsMut<[u8]> for Bytes {
    fn as_mut(&mut self) -> &mut [u8] {
        self.as_mut_slice()
    }
}

impl ops::Add for Bytes {
    type Output = Bytes;

    fn add(mut self, rhs: Bytes) -> Self::Output {
        self += rhs;
        self
    }
}

impl ops::AddAssign for Bytes {
    fn add_assign(&mut self, rhs: Bytes) {
        self.extend(rhs.iter().cloned())
    }
}

impl ops::Index<usize> for Bytes {
    type Output = u8;

    fn index(&self, idx: usize) -> &u8 {
        self.get(idx).expect("Index out of range")
    }
}

impl ops::IndexMut<usize> for Bytes {
    fn index_mut(&mut self, idx: usize) -> &mut u8 {
        self.get_mut(idx).expect("Index out of range")
    }
}

impl<'a> IntoIterator for &'a Bytes {
    type Item = &'a u8;
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a> IntoIterator for &'a mut Bytes {
    type Item = &'a mut u8;
    type IntoIter = IterMut<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = &'a u8;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

impl<'a> Iterator for IterMut<'a> {
    type Item = &'a mut u8;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}
