//! `Bool` and associated items.

use super::Type;
use std::ops;

/// The type literal associated with this primitive type.
pub const TYPE: Type = Type::BOOL;

/// The literal value `true`.
pub const TRUE: Bool = Bool::from_bool(true);

/// The literal value `false`.
pub const FALSE: Bool = Bool::from_bool(false);

/// `bool` - A boolean type with two literals, `false` and `true`.
///
/// Bitwise operators `&`, `|`, `^`, `!` and their corresponding assignment operators are defined
/// on booleans as the basic logical operations AND, OR, XOR, and NOT respectively. Their behavior
/// is equivalent to the corresponding operators in Rho.
///
/// # Examples
///
/// (TODO for when language is implemented)
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub struct Bool(bool);

impl Bool {
    /// The type literal associated with this primitive type.
    pub const TYPE: Type = TYPE;

    /// The literal value `true`.
    pub const TRUE: Bool = TRUE;

    /// The literal value `false`.
    pub const FALSE: Bool = FALSE;

    /// Converts the given boolean value to the equivalent Rho value.
    pub const fn from_bool(x: bool) -> Bool {
        Bool(x)
    }

    /// Converts this value into the equivalent Rust value.
    pub const fn into_bool(self) -> bool {
        self.0
    }
}

impl From<bool> for Bool {
    fn from(x: bool) -> Bool {
        Bool::from_bool(x)
    }
}

impl From<Bool> for bool {
    fn from(x: Bool) -> bool {
        x.into_bool()
    }
}

impl ops::BitAnd for Bool {
    type Output = Bool;

    fn bitand(self, rhs: Bool) -> Self::Output {
        Bool(self.0 & rhs.0)
    }
}

impl ops::BitAndAssign for Bool {
    fn bitand_assign(&mut self, rhs: Bool) {
        self.0 &= rhs.0;
    }
}

impl ops::BitOr for Bool {
    type Output = Bool;

    fn bitor(self, rhs: Bool) -> Self::Output {
        Bool(self.0 | rhs.0)
    }
}

impl ops::BitOrAssign for Bool {
    fn bitor_assign(&mut self, rhs: Bool) {
        self.0 |= rhs.0;
    }
}

impl ops::BitXor for Bool {
    type Output = Bool;

    fn bitxor(self, rhs: Bool) -> Self::Output {
        Bool(self.0 ^ rhs.0)
    }
}

impl ops::BitXorAssign for Bool {
    fn bitxor_assign(&mut self, rhs: Bool) {
        self.0 ^= rhs.0;
    }
}

impl ops::Not for Bool {
    type Output = Bool;

    fn not(self) -> Self::Output {
        Bool(!self.0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn constants() {
        assert_eq!(
            [Bool::from_bool(true), Bool::from_bool(false)],
            [TRUE, FALSE],
        );
        assert_eq!([TRUE.into_bool(), FALSE.into_bool()], [true, false]);
    }

    #[test]
    fn and() {
        assert_eq!(
            [FALSE & FALSE, FALSE & TRUE, TRUE & TRUE, TRUE & FALSE],
            [FALSE, FALSE, TRUE, FALSE],
        );
    }

    #[test]
    fn or() {
        assert_eq!(
            [FALSE | FALSE, FALSE | TRUE, TRUE | TRUE, TRUE | FALSE],
            [FALSE, TRUE, TRUE, TRUE],
        );
    }

    #[test]
    fn xor() {
        assert_eq!(
            [FALSE ^ FALSE, FALSE ^ TRUE, TRUE ^ TRUE, TRUE ^ FALSE],
            [FALSE, TRUE, FALSE, TRUE],
        );
    }

    #[test]
    fn not() {
        assert_eq!([!FALSE, !TRUE], [TRUE, FALSE]);
    }
}
