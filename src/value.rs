//! The dynamic `Value` type.

use crate::error::Error;
use crate::types::*;
use std::cmp::Ordering;

/// A meta type that can hold a value of any type.
#[derive(Debug, PartialEq, Clone)]
pub enum Value {
    /// `null`
    Null(Null),

    /// `bool`
    Bool(Bool),

    /// `int`
    Int(Int),

    /// `float`
    Float(Float),

    /// `bytes`
    Bytes(Bytes),

    /// `str`
    Str(Str),

    /// `list`
    List(List),

    /// `table`
    Table(Table),

    /// `function`
    Function(Function),

    /// `type`
    Type(Type),
}

impl Value {
    pub(crate) fn expected_type(&self, desc: &str) -> Error {
        Error::new(format!(
            "Unexpected type `{}` (expected `{}`",
            self.type_of().literal_name(),
            desc
        ))
    }

    /// Obtains the type of this value.
    pub fn type_of(&self) -> Type {
        match self {
            Value::Null(..) => Null::TYPE,
            Value::Bool(..) => Bool::TYPE,
            Value::Int(..) => Int::TYPE,
            Value::Float(..) => Float::TYPE,
            Value::Bytes(..) => Bytes::TYPE,
            Value::Str(..) => Str::TYPE,
            Value::List(..) => List::TYPE,
            Value::Table(..) => Table::TYPE,
            Value::Function(..) => Function::TYPE,
            Value::Type(..) => Type::TYPE,
        }
    }

    /// The addition operator, `a + b`.
    pub fn add(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Int(a), Int(b)) => Ok((a.clone() + b.clone()).into()),
            (Float(a), Float(b)) => Ok((a.clone() + b.clone()).into()),
            (Bytes(a), Bytes(b)) => Ok((a.clone() + b.clone()).into()),
            (Str(a), Str(b)) => Ok((a.clone() + b.clone()).into()),
            (List(a), List(b)) => Ok((a.clone() + b.clone()).into()),
            (Table(a), Table(b)) => Ok((a.clone() + b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a + b", a, b)),
        }
    }

    /// The subtraction operator, `a - b`.
    pub fn sub(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Int(a), Int(b)) => Ok((a.clone() + b.clone()).into()),
            (Float(a), Float(b)) => Ok((a.clone() + b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a - b", a, b)),
        }
    }

    /// The multiplication operator, `a * b`.
    pub fn mul(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Int(a), Int(b)) => Ok((a.clone() * b.clone()).into()),
            (Float(a), Float(b)) => Ok((a.clone() * b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a * b", a, b)),
        }
    }

    /// The division operator, `a / b`.
    pub fn div(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Int(a), Int(b)) => Ok((a.clone() / b.clone()).into()),
            (Float(a), Float(b)) => Ok((a.clone() / b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a / b", a, b)),
        }
    }

    /// The remainder operator, `a % b`.
    pub fn rem(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Int(a), Int(b)) => Ok((a.clone() % b.clone()).into()),
            (Float(a), Float(b)) => Ok((a.clone() % b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a % b", a, b)),
        }
    }

    /// The left-shift operator, `a << b`.
    pub fn shl(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Int(a), Int(b)) => Ok((a.clone() << b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a << b", a, b)),
        }
    }

    /// The (arithmetic) right-shift operator, `a >>> b`.
    pub fn shr(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Int(a), Int(b)) => Ok((a.clone() >> b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a >> b", a, b)),
        }
    }

    /// The bitwise-OR operator, `a | b`.
    pub fn or(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Bool(a), Bool(b)) => Ok((a.clone() | b.clone()).into()),
            (Int(a), Int(b)) => Ok((a.clone() | b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a | b", a, b)),
        }
    }

    /// The bitwise-AND operator, `a & b`.
    pub fn and(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Bool(a), Bool(b)) => Ok((a.clone() & b.clone()).into()),
            (Int(a), Int(b)) => Ok((a.clone() & b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a & b", a, b)),
        }
    }

    /// The bitwise-XOR operator, `a ^ b`.
    pub fn xor(&self, other: &Value) -> Result<Value, Error> {
        use self::Value::*;
        match (self, other) {
            (Bool(a), Bool(b)) => Ok((a.clone() ^ b.clone()).into()),
            (Int(a), Int(b)) => Ok((a.clone() ^ b.clone()).into()),
            (a, b) => Err(Error::binary_operator("a ^ b", a, b)),
        }
    }

    /// The negation operator, `-a`.
    pub fn neg(&self) -> Result<Value, Error> {
        use self::Value::*;
        match self {
            Int(a) => Ok((-a.clone()).into()),
            Float(a) => Ok((-a.clone()).into()),
            a => Err(Error::unary_operator("-a", a)),
        }
    }

    /// The bitwise-NOT operator, `!a`.
    pub fn not(&self) -> Result<Value, Error> {
        use self::Value::*;
        match self {
            Bool(a) => Ok((!a.clone()).into()),
            Int(a) => Ok((!a.clone()).into()),
            a => Err(Error::unary_operator("!a", a)),
        }
    }

    /// The generic comparison function, used by `<`, `>`, `<=`, `>=`.
    pub fn cmp(&self, other: &Value) -> Result<Option<Ordering>, Error> {
        use self::Value::*;
        match (self, other) {
            (Null(a), Null(b)) => Ok(a.partial_cmp(b)),
            (Bool(a), Bool(b)) => Ok(a.partial_cmp(b)),
            (Int(a), Int(b)) => Ok(a.partial_cmp(b)),
            (Float(a), Float(b)) => Ok(a.partial_cmp(b)),
            (Bytes(a), Bytes(b)) => Ok(a.partial_cmp(b)),
            (Str(a), Str(b)) => Ok(a.partial_cmp(b)),
            (a, b) => Err(Error::binary_operator("cmp(a, b)", a, b)),
        }
    }

    /// The less-than operator `a < b`.
    pub fn lt(&self, other: &Value) -> Result<Bool, Error> {
        let ordering = self
            .cmp(other)
            .map_err(|_| Error::binary_operator("a < b", self, other))?;

        Ok(match ordering {
            Some(Ordering::Less) => true,
            _ => false,
        }
        .into())
    }

    /// The greater-than operator `a > b`.
    pub fn gt(&self, other: &Value) -> Result<Bool, Error> {
        let ordering = self
            .cmp(other)
            .map_err(|_| Error::binary_operator("a > b", self, other))?;

        Ok(match ordering {
            Some(Ordering::Greater) => true,
            _ => false,
        }
        .into())
    }

    /// The less-equal operator `a <= b`.
    pub fn le(&self, other: &Value) -> Result<Bool, Error> {
        let ordering = self
            .cmp(other)
            .map_err(|_| Error::binary_operator("a <= b", self, other))?;

        Ok(match ordering {
            Some(Ordering::Less) | Some(Ordering::Equal) => true,
            _ => false,
        }
        .into())
    }

    /// The greater-equal operator `a >= b`.
    pub fn ge(&self, other: &Value) -> Result<Bool, Error> {
        let ordering = self
            .cmp(other)
            .map_err(|_| Error::binary_operator("a >= b", self, other))?;

        Ok(match ordering {
            Some(Ordering::Greater) | Some(Ordering::Equal) => true,
            _ => false,
        }
        .into())
    }

    /// The infallible equality operator `a == b`.
    pub fn eq(&self, other: &Value) -> Bool {
        use self::Value::*;
        match (self, other) {
            (Null(a), Null(b)) => a == b,
            (Bool(a), Bool(b)) => a == b,
            (Int(a), Int(b)) => a == b,
            (Float(a), Float(b)) => a == b,
            (Bytes(a), Bytes(b)) => a == b,
            (Str(a), Str(b)) => a == b,
            (List(a), List(b)) => a == b,
            (Table(a), Table(b)) => a == b,
            (Function(a), Function(b)) => a == b,
            (Type(a), Type(b)) => a == b,
            _ => false,
        }
        .into()
    }

    /// The infallible inequality operator `a != b`.
    pub fn ne(&self, other: &Value) -> Bool {
        !self.eq(other)
    }

    /// The length function `len a`.
    pub fn len(&self) -> Result<Int, Error> {
        match self {
            Value::Bytes(a) => Ok(Int::from(a.len() as i128)),
            Value::Str(a) => Ok(Int::from(a.len() as i128)),
            Value::List(a) => Ok(Int::from(a.len() as i128)),
            Value::Table(a) => Ok(Int::from(a.len() as i128)),
            _ => Err(Error::unary_operator("len", self)),
        }
    }
}

macro_rules! convert_value {
    ($($x:ty, $xi:ident;)*) => {$(
        impl From<$x> for Value {
            fn from(x: $x) -> Value {
                Value::$xi(x)
            }
        }

        impl std::convert::TryFrom<Value> for $x {
            type Error = Value;

            fn try_from(value: Value) -> Result<$x, Value> {
                if let Value::$xi(x) = value {
                    Ok(x)
                } else {
                    Err(value)
                }
            }
        }
    )*}
}

convert_value! {
    Null, Null;
    Bool, Bool;
    Int, Int;
    Float, Float;
    Bytes, Bytes;
    Str, Str;
    List, List;
    Table, Table;
    Function, Function;
    Type, Type;
}
