//! Rho is a dynamically-typed scripting language, designed mainly for
//! scripting extensions via its Rust API, similar to the C API for Lua.

#![warn(missing_docs)]

pub mod error;
pub mod expression;
pub mod types;
pub mod value;
