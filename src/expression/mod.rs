//! The expression-tree interpretation of the Rho language.
//!
//! Most language components are treated as an expression that can be evaluated to either produce a
//! value or fail with an error. This property is defined by the `Expression` trait. Since most
//! expressions are combinations of other expressions, they can then be defined as generic across
//! other expression types.

use crate::error::Error;
use crate::types::*;
use crate::value::Value;
use std::cmp::Ordering;
use std::collections::HashMap;

/// The possible ways for an expression to exit.
pub enum Exit {
    /// The expression finished normally.
    Normal(Value),

    /// The expression exited early with a return statement.
    Return(Value),

    /// The expression exited early with a break statement.
    Break(Value),

    /// The expression exited early with a continue statement.
    Continue,

    /// The expression exited early with an error.
    Error(Error),
}

impl From<Result<Value, Error>> for Exit {
    fn from(result: Result<Value, Error>) -> Exit {
        match result {
            Ok(value) => Exit::Normal(value),
            Err(error) => Exit::Error(error),
        }
    }
}

macro_rules! try_expr {
    ($expr:expr, $scope:expr) => {
        match $expr.evaluate($scope) {
            $crate::expression::Exit::Normal(value) => value,
            other => {
                return other;
            }
        }
    };
    ($expr:expr,) => {
        try_expr!($expr)
    };
}

/// A function's scope that holds the local variables and return state.
pub struct Scope {
    variables: HashMap<Str, Value>,
    return_value: Option<Value>,
}

impl Scope {
    /// Creates a new scope with an empty variable map and no return value.
    pub fn new() -> Scope {
        Scope {
            variables: HashMap::new(),
            return_value: Some(Null::NULL.into()),
        }
    }

    /// Creates a new scope with an empty variable map and no return value. Pre-allocates the
    /// variable map so it can hold the given number of variables without reallocating.
    pub fn with_capacity(capacity: usize) -> Scope {
        Scope {
            variables: HashMap::with_capacity(capacity),
            return_value: Some(Null::NULL.into()),
        }
    }

    /// Gets the current value of this variable if it exists.
    pub fn get_variable(&self, name: &Str) -> Option<&Value> {
        self.variables.get(name)
    }

    /// Sets the value of this variable, replacing any existing value.
    pub fn set_variable(&mut self, name: Str, value: Value) {
        self.variables.insert(name, value);
    }

    /// Gets the current return value, if it exists.
    pub fn get_return_value(&self) -> Option<&Value> {
        self.return_value.as_ref()
    }

    /// Consumes `self`, giving ownership of the return value.
    pub fn into_return_value(self) -> Option<Value> {
        self.return_value
    }

    /// Sets the return value to the given value.
    ///
    /// # Panics
    ///
    /// Panics in debug mode if a return value has already been set, as a function can only accept
    /// at most one return value, and the return statement may only execute once in a scope, after
    /// which the function will stop executing.
    pub fn set_return_value(&mut self, value: Value) {
        debug_assert!(self.return_value.is_none());
        self.return_value = Some(value);
    }
}

/// An expression that can be evaluated.
pub trait Expression {
    /// Evaluates the expression within the given scope.
    fn evaluate(&self, scope: &mut Scope) -> Exit;
}

impl<'a, T> Expression for &'a T
where
    T: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        T::evaluate(*self, scope)
    }
}

/// An expression that holds a literal (constant) value, like `'foo'`, `8.0`, `[9, 'bar']`.
pub struct Literal {
    value: Value,
}

impl Literal {
    /// Constructs a literal that will evaluate to the given value.
    pub fn new(value: Value) -> Literal {
        Literal { value }
    }
}

impl Expression for Literal {
    fn evaluate(&self, _scope: &mut Scope) -> Exit {
        Exit::Normal(self.value.clone())
    }
}

/// An expression that takes its value from a variable in scope, like `x` or `var`.
pub struct Variable {
    name: Str,
}

impl Variable {
    /// Constructs a variable expression that will use the given name.
    pub fn new(name: Str) -> Variable {
        Variable { name }
    }
}

impl Expression for Variable {
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        scope
            .get_variable(&self.name)
            .cloned()
            .ok_or_else(|| {
                Error::new(format!(
                    "Variable not defined in this scope: `{}`",
                    self.name.as_str(),
                ))
            })
            .into()
    }
}

/// A block of statements that may produce a value, like `{ x += 2; x }`.
pub struct Block<E, F>
where
    for<'a> &'a E: IntoIterator,
    for<'a> <&'a E as IntoIterator>::Item: Expression,
{
    statements: E,
    result: F,
}

impl<E, F> Block<E, F>
where
    for<'a> &'a E: IntoIterator,
    for<'a> <&'a E as IntoIterator>::Item: Expression,
{
    /// Constructs a block with the given statements and result expression.
    pub fn new(statements: E, result: F) -> Block<E, F> {
        Block { statements, result }
    }
}

impl<E, F> Expression for Block<E, F>
where
    for<'a> &'a E: IntoIterator,
    for<'a> <&'a E as IntoIterator>::Item: Expression,
    F: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        for expr in &self.statements {
            try_expr!(expr, scope);
        }
        self.result.evaluate(scope)
    }
}

macro_rules! unary_op {
    ($($name:ident, $func:expr, $doc:expr;)*) => {$(

        #[doc = $doc]
        pub struct $name<E> {
            expr: E,
        }

        impl<E> $name<E> {
            /// Constructs the expression using the given operand.
            pub fn new(expr: E) -> $name<E> {
                $name {
                    expr,
                }
            }
        }

        impl<E> Expression for $name<E>
        where
            E: Expression,
        {
            fn evaluate(&self, scope: &mut Scope) -> Exit {
                $func(&try_expr!(self.expr, scope))
                    .map(Value::from)
                    .into()
            }
        }
    )*}
}

macro_rules! binary_op {
    ($($name:ident, $func:expr, $doc:expr;)*) => {$(

        #[doc = $doc]
        pub struct $name<E, F> {
            lhs: E,
            rhs: F,
        }

        impl<E, F> $name<E, F> {
            /// Constructs the expression using the given operands.
            pub fn new(lhs: E, rhs: F) -> $name<E, F> {
                $name {
                    lhs,
                    rhs,
                }
            }
        }

        impl<E, F> Expression for $name<E, F>
        where
            E: Expression,
            F: Expression,
        {
            fn evaluate(&self, scope: &mut Scope) -> Exit {
                $func(&try_expr!(self.lhs, scope), &try_expr!(self.rhs, scope))
                    .map(Value::from)
                    .into()
            }
        }
    )*}
}

macro_rules! compare_op {
    ($($name:ident, $func:expr, $doc:expr;)*) => {$(

        #[doc = $doc]
        pub struct $name<E, F> {
            lhs: E,
            rhs: F,
        }

        impl<E, F> $name<E, F> {
            /// Constructs the expression using the given operands.
            pub fn new(lhs: E, rhs: F) -> $name<E, F> {
                $name {
                    lhs,
                    rhs,
                }
            }
        }

        impl<E, F> Expression for $name<E, F>
        where
            E: Expression,
            F: Expression,
        {
            fn evaluate(&self, scope: &mut Scope) -> Exit {
                $func(&try_expr!(self.lhs, scope), &try_expr!(self.rhs, scope))
                    .map(Value::from)
                    .into()
            }
        }
    )*}
}

macro_rules! equal_op {
    ($($name:ident, $func:expr, $doc:expr;)*) => {$(

        #[doc = $doc]
        pub struct $name<E, F> {
            lhs: E,
            rhs: F,
        }

        impl<E, F> $name<E, F> {
            /// Constructs the expression using the given operands.
            pub fn new(lhs: E, rhs: F) -> $name<E, F> {
                $name {
                    lhs,
                    rhs,
                }
            }
        }

        impl<E, F> Expression for $name<E, F>
        where
            E: Expression,
            F: Expression,
        {
            fn evaluate(&self, scope: &mut Scope) -> Exit {
                Exit::Normal($func(&try_expr!(self.lhs, scope), &try_expr!(self.rhs, scope)).into())
            }
        }
    )*}
}

unary_op! {
    Neg, Value::neg, "The `neg a` function (infix `-a`).";
    Not, Value::not, "The `not a` function (infix `!a`).";
    Len, Value::len, "The `len a` function.";
}

binary_op! {
    Add, Value::add, "The `add a b` function (infix `a + b`).";
    Sub, Value::sub, "The `sub a b` function (infix `a - b`).";
    Mul, Value::mul, "The `mul a b` function (infix `a * b`).";
    Div, Value::div, "The `div a b` function (infix `a / b`).";
    Rem, Value::rem, "The `rem a b` function (infix `a % b`).";
    And, Value::and, "The `and a b` function (infix `a & b`).";
    Or, Value::or, "The `or a b` function (infix `a | b`).";
    Xor, Value::xor, "The `xor a b` function (infix `a ^ b`).";
    Shl, Value::shl, "The `shl a b` function (infix `a << b`).";
    Shr, Value::shr, "The `shr a b` function (infix `a >> b`).";
}

/// The `cmp a b` function.
///
/// Maps `Option<Ordering>` outputs from `Value::cmp` into values using the following table:
///
/// - `Some(Greater) => 1`
/// - `Some(Less) => -1`
/// - `Some(Equal) => 0`
/// - `None` => null`
pub struct Cmp<E, F> {
    lhs: E,
    rhs: F,
}

impl<E, F> Cmp<E, F> {
    /// Constructs the expression using the given operands.
    pub fn new(lhs: E, rhs: F) -> Cmp<E, F> {
        Cmp { lhs, rhs }
    }
}

impl<E, F> Expression for Cmp<E, F>
where
    E: Expression,
    F: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        match Value::cmp(&try_expr!(self.lhs, scope), &try_expr!(self.rhs, scope)) {
            Ok(Some(Ordering::Greater)) => Exit::Normal(Int::from(1).into()),
            Ok(Some(Ordering::Less)) => Exit::Normal(Int::from(-1).into()),
            Ok(Some(Ordering::Equal)) => Exit::Normal(Int::from(0).into()),
            Ok(None) => Exit::Normal(Null::NULL.into()),
            Err(error) => Exit::Error(error),
        }
    }
}

compare_op! {
    LessThan, Value::lt, "The `lt a b` function (infix `a < b`).";
    GreaterThan, Value::gt, "The `gt a b` function (infix `a < b`).";
    LessEqual, Value::le, "The `le a b` function (infix `a < b`).";
    GreaterEqual, Value::ge, "The `ge a b` function (infix `a < b`).";
}

equal_op! {
    Equal, Value::eq, "The `eq a b` function (infix `a == b`).";
    NotEqual, Value::ne, "The `ne a b` function (infix `a != b`).";
}

/// The list constructor `[a + b, ..foo, x[y]]`.
pub struct ListCons<E, F, G>
where
    for<'a> &'a E: IntoIterator<Item = ListPart<F, G>>,
    F: Expression,
    G: Expression,
{
    parts: E,
}

/// The kinds of parts that a list construction can have.
pub enum ListPart<E, F> {
    /// Produces a single element with the given value.
    Element(E),

    /// Unpacks the list, producing all of its elements in order.
    Unpack(F),
}

impl<E, F, G> ListCons<E, F, G>
where
    for<'a> &'a E: IntoIterator<Item = ListPart<F, G>>,
    F: Expression,
    G: Expression,
{
    /// Creates a list constructor from the given list of parts.
    pub fn new(parts: E) -> ListCons<E, F, G> {
        ListCons { parts }
    }
}

impl<E, F, G> Expression for ListCons<E, F, G>
where
    for<'a> &'a E: IntoIterator<Item = ListPart<F, G>>,
    F: Expression,
    G: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        let parts_iter = self.parts.into_iter();
        let (lower, _) = parts_iter.size_hint();
        let mut parts = Vec::with_capacity(lower.saturating_add(1));

        for part in parts_iter {
            let result = match part {
                ListPart::Element(expr) => ListPart::Element(try_expr!(expr, scope)),
                ListPart::Unpack(expr) => match try_expr!(expr, scope) {
                    Value::List(list) => ListPart::Unpack(list),
                    other => return Exit::Error(other.expected_type("list")),
                },
            };
            parts.push(result);
        }

        let len = parts
            .iter()
            .map(|part| match part {
                ListPart::Element(..) => 1,
                ListPart::Unpack(list) => list.len(),
            })
            .sum();

        let mut vec = Vec::with_capacity(len);

        for part in parts {
            match part {
                ListPart::Element(e) => vec.push(e),
                ListPart::Unpack(e) => vec.extend(e.iter().cloned()),
            }
        }

        Exit::Normal(List::from(vec).into())
    }
}

/// The table constructor `{'spam': true, ..prev, 'foo'}`.
pub struct TableCons<E, F, G, H>
where
    for<'a> &'a E: IntoIterator<Item = TablePart<F, G, H>>,
    F: Expression,
    G: Expression,
    H: Expression,
{
    parts: E,
}

/// The kinds of parts that a table construction can have.
pub enum TablePart<E, F, G> {
    /// Produces an entry with the given key and value.
    Entry(E, F),

    /// "Unpacks" the given table, producing all of its entries.
    Unpack(G),
}

impl<E, F, G, H> TableCons<E, F, G, H>
where
    for<'a> &'a E: IntoIterator<Item = TablePart<F, G, H>>,
    F: Expression,
    G: Expression,
    H: Expression,
{
    /// Creates the table constructor from the given list of parts.
    pub fn new(parts: E) -> TableCons<E, F, G, H> {
        TableCons { parts: parts }
    }
}

impl<E, F, G, H> Expression for TableCons<E, F, G, H>
where
    for<'a> &'a E: IntoIterator<Item = TablePart<F, G, H>>,
    F: Expression,
    G: Expression,
    H: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        let parts_iter = self.parts.into_iter();
        let (lower, _) = parts_iter.size_hint();
        let mut parts = Vec::with_capacity(lower.saturating_add(1));

        for part in parts_iter {
            let result = match part {
                TablePart::Entry(key, val) => TablePart::Entry(
                    match try_expr!(key, scope) {
                        Value::Str(s) => s,
                        other => return Exit::Error(other.expected_type("str")),
                    },
                    try_expr!(val, scope),
                ),
                TablePart::Unpack(expr) => match try_expr!(expr, scope) {
                    Value::Table(table) => TablePart::Unpack(table),
                    other => return Exit::Error(other.expected_type("table")),
                },
            };
            parts.push(result);
        }

        let len = parts
            .iter()
            .map(|part| match part {
                TablePart::Entry(..) => 1,
                TablePart::Unpack(table) => table.len(),
            })
            .sum();

        let mut map = HashMap::with_capacity(len);

        for part in parts {
            match part {
                TablePart::Entry(k, v) => {
                    map.insert(k, v);
                }
                TablePart::Unpack(e) => {
                    map.extend(e.iter().map(|(a, b)| (a.clone(), b.clone())));
                }
            }
        }

        Exit::Normal(Table::from_hashmap(map).into())
    }
}

/// A sequence of if and else-if statements, followed by an optional else
pub struct If<E, F, G, H>
where
    for<'a> &'a E: IntoIterator<Item = (F, G)>,
    F: Expression,
    G: Expression,
    H: Expression,
{
    if_clauses: E,
    else_expr: H,
}

impl<E, F, G, H> If<E, F, G, H>
where
    for<'a> &'a E: IntoIterator<Item = (F, G)>,
    F: Expression,
    G: Expression,
    H: Expression,
{
    /// Constructs the expression from the given if-chain and else clause.
    pub fn new(if_clauses: E, else_expr: H) -> If<E, F, G, H> {
        If {
            if_clauses,
            else_expr,
        }
    }
}

impl<E, F, G, H> Expression for If<E, F, G, H>
where
    for<'a> &'a E: IntoIterator<Item = (F, G)>,
    F: Expression,
    G: Expression,
    H: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        // Find the first condition that evaluates to true, and evaluate its corresponding block:
        for (cond, expr) in &self.if_clauses {
            let cond_val = match try_expr!(cond, scope) {
                Value::Bool(b) => b,
                other => return Exit::Error(other.expected_type("bool")),
            };

            if cond_val == Bool::TRUE {
                return expr.evaluate(scope);
            }
        }
        // Otherwise, evaluate the else block:
        self.else_expr.evaluate(scope)
    }
}

/// A `return` statement that terminates a function early, producing a value.
pub struct Return<E> {
    result: E,
}

impl<E> Return<E> {
    /// Constructs a return statement with the given result expression.
    pub fn new(result: E) -> Return<E> {
        Return { result }
    }
}

impl<E> Expression for Return<E>
where
    E: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        Exit::Return(try_expr!(self.result, scope))
    }
}

/// A `break` statement that terminates a loop structure early, producing a value.
pub struct Break<E> {
    result: E,
}

impl<E> Break<E> {
    /// Constructs a break statement with the given result expression.
    pub fn new(result: E) -> Break<E> {
        Break { result }
    }
}

impl<E> Expression for Break<E>
where
    E: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        Exit::Break(try_expr!(self.result, scope))
    }
}

/// A `continue` statement that terminates a loop iteration early.
pub struct Continue(());

impl Continue {
    /// Constructs a continue statement.
    pub fn new() -> Continue {
        Continue(())
    }
}

impl Expression for Continue {
    fn evaluate(&self, _scope: &mut Scope) -> Exit {
        Exit::Continue
    }
}

/// A loop that repeats unconditionally until a break occurs.
pub struct Loop<E>
where
    E: Expression,
{
    loop_expr: E,
}

impl<E> Loop<E>
where
    E: Expression,
{
    /// Constructs a loop with a given body expression.
    pub fn new(loop_expr: E) -> Loop<E> {
        Loop { loop_expr }
    }
}

impl<E> Expression for Loop<E>
where
    E: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        loop {
            match self.loop_expr.evaluate(scope) {
                Exit::Break(value) => return Exit::Normal(value),
                Exit::Error(error) => return Exit::Error(error),
                _ => {}
            }
        }
    }
}

/// A `while` expression that loops while the condition stays true. Can produce a result value
/// either through an early `break` or from a normal exit via the `else` clause.
pub struct While<E, F, G>
where
    E: Expression,
    F: Expression,
    G: Expression,
{
    cond: E,
    loop_expr: F,
    else_expr: G,
}

impl<E, F, G> While<E, F, G>
where
    E: Expression,
    F: Expression,
    G: Expression,
{
    /// Constructs a `while` expression from the condition, loop body, and else clause.
    pub fn new(cond: E, loop_expr: F, else_expr: G) -> While<E, F, G> {
        While {
            cond,
            loop_expr,
            else_expr,
        }
    }
}

impl<E, F, G> Expression for While<E, F, G>
where
    E: Expression,
    F: Expression,
    G: Expression,
{
    fn evaluate(&self, scope: &mut Scope) -> Exit {
        loop {
            let cond = match try_expr!(self.cond, scope) {
                Value::Bool(b) => b,
                other => return Exit::Error(other.expected_type("bool")),
            };

            if cond != Bool::TRUE {
                return self.else_expr.evaluate(scope);
            }
            match self.loop_expr.evaluate(scope) {
                Exit::Break(value) => return Exit::Normal(value),
                Exit::Error(error) => return Exit::Error(error),
                _ => {}
            }
        }
    }
}
