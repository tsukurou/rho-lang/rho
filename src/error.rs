//! Error types.

use crate::value::Value;
use std::error::Error as StdError;
use std::fmt;

/// The crate error type.
#[derive(Debug)]
pub struct Error {
    message: String,
    source: Option<Box<(dyn StdError + 'static)>>,
}

impl Error {
    #[allow(dead_code)]
    pub(crate) fn new<S>(message: S) -> Error
    where
        S: fmt::Display,
    {
        Error {
            message: format!("{}", message),
            source: None,
        }
    }

    #[allow(dead_code)]
    pub(crate) fn with_source<S, E>(message: S, source: E) -> Error
    where
        S: fmt::Display,
        E: StdError + 'static,
    {
        Error {
            message: format!("{}", message),
            source: Some(Box::new(source)),
        }
    }

    pub(crate) fn unary_operator(operation: &str, a: &Value) -> Error {
        Error::new(format!(
            "The operation `{}` cannot be applied to the type `{}`",
            operation,
            a.type_of().literal_name(),
        ))
    }

    pub(crate) fn binary_operator(operation: &str, a: &Value, b: &Value) -> Error {
        Error::new(format!(
            "The operation `{}` cannot be applied to the types `{}` and `{}`",
            operation,
            a.type_of().literal_name(),
            b.type_of().literal_name(),
        ))
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&self.message)
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        self.source.as_ref().map(Box::as_ref)
    }
}
